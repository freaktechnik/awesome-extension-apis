# Awesome Browser Extension APIs [![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

> Awesome cross-extension APIs that enable browser extensions to integrate with each other.

## Contents
- [Resources](#resources)
- [Applications](#applications)
  - [Streamlink Helper](#streamlink-helper)
- [Features](#features)
  - [Notification Sound](#notification-sound)
  - [Multi-Account Containers](#multi-account-containers)
  - [Temporary Containers](#temporary-containers)
  - [Wikipedia Peek](#wikipedia-peek)
  
## Resources
- [Overview of why and how extensions can integrate](https://humanoids.be/log/2017/11/browser-extensions-work-together/)

## Applications

### [Streamlink Helper](https://github.com/plneappl/streamlink-helper#invoking-streamlink-from-another-extension)
Lets other extensions launch Streamlink via its integration.

#### Supports
- [Firefox](https://addons.mozilla.org/firefox/addon/streamlink_helper/)
 
## Features
 
### [Notification Sound](https://github.com/freaktechnik/notification-sounds#extension-integration)
Plays a user defined notification sound when extensions inform it that they're showing a notification.

#### Supports
- [Firefox](https://addons.mozilla.org/firefox/addon/notification-sound/?src=github)

### [Multi-Account Containers](https://github.com/mozilla/multi-account-containers/wiki/API)
Get information about assigned default containers for URLs.

#### Supports
- [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/multi-account-containers/)

### [Temporary Containers](https://github.com/stoically/temporary-containers/wiki/API)
Open an URL in a temporary container and check if a container belongs to the extension.

#### Supports
- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/)

### [Wikipedia Peek](https://github.com/NiklasGollenstede/wikipedia-peek/blob/master/plugin/index.js)
Shows a link preview on hower. Other extensions can register plugins based on the given template to provide preview information.

#### Supports
- [Firefox](https://addons.mozilla.org/firefox/addon/wikipedia-peek/?src=github)
- [Chrome](https://chrome.google.com/webstore/detail/wikipedia-peek/planddpadjimakmjdpnolpjjphhooiej)
- [Opera](https://addons.opera.com/en/extensions/details/wikipedia-peek/)
